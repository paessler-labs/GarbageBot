import sys
import os
import re

import requests
import gitlab
import urllib3

import logging
from jaeger_client import Config

# Disabling certificate warnings in case of self signed certificates (only during runtime)
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class GitlabServer(object):
    def __init__(self, tracer=None, span=None):
        self.span = span
        self.tracer = tracer
        self.settings = self.read_settings()
        self.gl_server_base = self.settings['gl_server_base']
        self.gl_auth_token = self.settings['gl_auth_token']
        self.gl_base_project_id = self.settings['gl_base_project_id']
        self.purge_all = self.settings['purge_all']
        self.keep_branch_images = self.settings['gl_keep_branch_images']
        self.gl_api = gitlab.Gitlab(self.gl_server_base, self.gl_auth_token, api_version='4', ssl_verify=False)
        self.request_headers = {
            'Private-Token': self.gl_auth_token
        }
        self.api_base_url = '{0}{1}'.format(self.gl_server_base, '/api/v4/')

    def get_registry_repositories_for_project(self):
        """
        This function returns all registry repositories for a given project
        :return: list of registries
        """
        with self.tracer.start_span('get registry repos', child_of=self.span) as gr_span:
            registries = []
            pr_endpoint = '{0}/projects/{1}/registry/repositories'.format(self.api_base_url, self.gl_base_project_id)
            gr_span.set_tag('http.url', pr_endpoint)
            # doing the initial request
            resp = requests.get(pr_endpoint, headers=self.request_headers)
            gr_span.set_tag('http.status_code', resp.status_code)
            for reg in resp.json():
                registries.append(reg)

            # handle api pagination
            while 'next' in resp.links:
                gr_span.log_kv({'event': 'pagination'})
                gr_span.set_tag('http.url', resp.links['next']['url'])
                resp = requests.get(resp.links['next']['url'], headers=self.request_headers)
                gr_span.set_tag('http.status_code', resp.status_code)
                for reg in resp.json():
                    registries.append(reg)
            gr_span.log_kv({'registries': registries})
            return registries

    def get_image_tags_for_registry_repository(self, registry_id):
        """
        This function returns all image tags for a given registry repository
        :param registry_id: the id of the registry to delete from
        :return: dict of image tags
        """
        with self.tracer.start_span('get image tags', child_of=self.span) as gt_span:
            tags = []
            rt_endpoint = '{0}/projects/{1}/registry/repositories/{2}/tags?per_page=100'.format(self.api_base_url,
                                                                                                self.gl_base_project_id,
                                                                                                registry_id)
            gt_span.set_tag('http.url', rt_endpoint)
            # doing the initial request
            resp = requests.get(rt_endpoint, headers=self.request_headers)
            gt_span.set_tag('http.status_code', resp.status_code)
            for tag in resp.json():
                tags.append(tag)

            # handle api pagination
            while 'next' in resp.links:
                gt_span.log_kv({'event': 'pagination'})
                gt_span.set_tag('http.url', resp.links['next']['url'])
                resp = requests.get(resp.links['next']['url'], headers=self.request_headers)
                gt_span.set_tag('http.status_code', resp.status_code)
                for tag in resp.json():
                    tags.append(tag)
            gt_span.log_kv({'tags': tags})
            return tags

    def get_project_branches(self):
        """
        This function returns all branches for a given project
        :return: list of branch objects
        """
        with self.tracer.start_span('get project branches', child_of=self.span) as gp_span:
            project = self.gl_api.projects.get(self.gl_base_project_id)
            return project.branches.list(all=True)

    def delete_single_image_tag(self, registry_id, tag_name, span=None):
        """
        This function deletes the given tag name from the given registry repository
        :param registry_id: the id of the registry to delete from
        :param tag_name: the name of the tag to delete
        :return: HTTP response object or DRY_RUN
        """
        dt_endpoint = '{0}/projects/{1}/registry/repositories/{2}/tags/{3}'.format(self.api_base_url,
                                                                                       self.gl_base_project_id,
                                                                                       registry_id,
                                                                                       tag_name)
        span.set_tag('http.url', dt_endpoint)
        if not self.settings['dry_run']:
            resp = requests.delete(dt_endpoint, headers=self.request_headers)
            span.set_tag('http.status_code', resp.status_code)
            return resp
        return 'DRY_RUN'

    def delete_single_registry_repository(self, registry_id, span=None):
        """
        This function deletes the given registry
        :param registry_id: the id of the registry to delete from
        :return: HTTP response object or DRY_RUN
        """
        dr_endpoint = '{0}projects/{1}/registry/repositories/{2}'.format(self.api_base_url,
                                                                             self.gl_base_project_id,
                                                                             registry_id)
        span.set_tag('http.url', dr_endpoint)
        if not self.settings['dry_run']:
            resp = requests.delete(dr_endpoint, headers=self.request_headers)
            span.set_tag('http.status_code', resp.status_code)
            return resp
        return 'DRY_RUN'

    def delete_bulk_image_tag(self, registry_id, older_than=None, span=None):
        """
        This function does bulk deleting of image tags based on certain rulesets
        This one is going to be ugly to test, as per the Gitlab API Documentation at
        https://docs.gitlab.com/ee/api/container_registry.html#delete-repository-tags-in-bulk
        this operation is only permitted once per hour...
        :param registry_id:
        :param older_than:
        :return: HTTP Response object or 'DRY_RUN'
        """
        # This one is going to be ugly to test, as per the Gitlab API Documentation at
        # https://docs.gitlab.com/ee/api/container_registry.html#delete-repository-tags-in-bulk
        # this operation is only permitted once per hour...
        dbt_endpoint = '{0}/projects/{1}/registry/repositories/{2}/tags'.format(self.api_base_url,
                                                                                self.gl_base_project_id,
                                                                                registry_id)
        span.set_tag('http.url', dbt_endpoint)
        if older_than:
            http_data = {
                "name_regex": ".*",
                "older_than": older_than
            }
        else:
            # this is purge all
            http_data = {
                "name_regex": ".*"
            }
            span.set_tag('http.get-params', http_data)
        if not self.settings['dry_run']:
            return requests.delete(dbt_endpoint, data=http_data, headers=self.request_headers)
        return 'DRY_RUN'

    def normalize_branch_name(self, branch_name, span=None):
        """
        This function creates a slug of the branchname (See Gitlab Docs for details)
        :param branch_name: branch name
        :return: string normalized branchname
        """
        with self.tracer.start_span('Normalizing branch {0}'.format(branch_name.lower()), child_of=span) as bn_span:
            norm_branch = re.sub(r'[^a-z0-9-]', '-', branch_name.lower())
            bn_span.log_kv({'normalized_branch_name': norm_branch})
            return norm_branch

    def filter_taglist_regex(self, re_filter=None, available_taglist=None, keep=False):
        """
        Filtering the taglist available from the gitlab registry by regex and return a filtered list
        :param re_filter: regex to match
        :param available_taglist:
        :param keep:
        :return:
        """
        filtered_taglist = []
        for tag in available_taglist:
            if keep:
                if not re.match(re_filter, tag['name']):
                    filtered_taglist.append(tag['name'])
            else:
                if re.match(re_filter, tag['name']):
                    filtered_taglist.append(tag['name'])

        return filtered_taglist

    def verify_sanitize_settings(self, settings_dict):
        """
        Function to verify the settings read from ENV. When a mandatory setting is missing, abort if a optional
        setting is missing, print configuration
        :param settings_dict: A dictionary containing the settings
        :return: True/False
        """
        with self.tracer.start_span('sanitize settings', child_of=self.span) as ss_span:
            for k, v in settings_dict.items():
                if v == 'NOT_SET':
                    ss_span.log('{0} not set. This setting is mandatory. Exiting!'.format(k))
                    print('{0} not set. This setting is mandatory. Exiting!'.format(k))
                    return False
                if not v:
                    print('{0} not set.'.format(k))
                if v and v.lower() == 'true':
                    settings_dict[k] = True
                if v and v.lower() == 'false':
                    settings_dict[k] = False
            ss_span.log_kv({'sanitized settings': settings_dict})
            return True

    def response_handling_del_request(self, response=None, image_tag=None, reg_repo_name=None, scope='tag', span=None):
        delete_msg = 'Deleting image {0}'.format(image_tag)
        delete_success_msg = 'Deleting image {0} successful'.format(image_tag)
        if not scope == 'tag':
            delete_msg = delete_success_msg = 'Bulk delete worked! For registry {0}'.format(reg_repo_name)
        if response == 'DRY_RUN':
            print('*' * 10 + ' DRY RUN ' + '*' * 10)
            print(delete_msg)
            print('*' * 10 + ' DRY RUN ' + '*' * 10)
            span.log_kv({'event': delete_msg, 'important': 'DRY RUN'})
        elif response.status_code == 200:
            span.log_kv({'event': delete_success_msg})
            print(delete_success_msg)
        elif response.status_code == 202:
            span.log_kv({'event': delete_success_msg})
            print(delete_success_msg)
        else:
            span.log_kv({'event': 'Something went wrong while deleting.',
                         'http_status': str(response.status_code),
                         'http_content': str(response.text)})
            print('Something went wrong while deleting.')
            print('HTTP Status Code: ' + str(response.status_code))
            print('Message: ' + str(response.text))

    def read_settings(self):
        """
        Function reads the settings from the environment
        :return: dict of settings
        """
        with self.tracer.start_span('read settings', child_of=self.span) as rs_span:
            settings_dict = {
                'gl_server_base': os.getenv('GL_SERVER_BASE', 'NOT_SET'),
                'gl_auth_token': os.getenv('GL_AUTH_TOKEN', 'NOT_SET'),
                'gl_base_project_id': os.getenv('GL_BASE_PROJECT_ID', 'NOT_SET'),
                'gl_keep_branch_images': os.getenv('GL_KEEP_BRANCH_IMAGES', 'True'),
                'gl_add_suffix_to_branch': os.getenv('GL_ADD_SUFFIX_TO_BRANCH', None),
                'gl_images_to_delete': os.getenv('GL_IMAGES_TO_DELETE', None),
                'gl_images_to_keep': os.getenv('GL_IMAGES_TO_KEEP', None),
                'gl_delete_older_than': os.getenv('GL_DELETE_OLDER_THAN', None),
                'gl_keep_latest_version': os.getenv('GL_KEEP_LATEST_VERSIONS', None),
                'gl_delete_by_regex': os.getenv('GL_DELETE_BY_REGEX', None),
                'gl_keep_by_regex': os.getenv('GL_KEEP_BY_REGEX', None),
                'gl_scope_registry': os.getenv('GL_SCOPE_REGISTRY', 'False'),
                'purge_all': os.getenv('PURGE_ALL', 'False'),
                'dry_run': os.getenv('DRY_RUN', 'True')
            }
            rs_span.log_kv({'Settings': settings_dict})
            if self.verify_sanitize_settings(settings_dict):
                return settings_dict
            else:
                sys.exit(1)
