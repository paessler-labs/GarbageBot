import sys
import os
import time
import logging
from jaeger_client import Config

from GarbageBotMK2 import GitlabServer


def init_tracer():
    jaeger_host = os.getenv('JAEGER_HOST', None)
    jaeger_port = os.getenv('JAEGER_PORT', None)
    tracer_cfg = {
        'sampler': {
            'type': 'const',
            'param': 1,
        },
        'logging': True
    }
    if jaeger_host and jaeger_port:
        tracer_cfg['local_agent'] = {
            'reporting_host': jaeger_host,
            'reporting_port': jaeger_port
        }
    config = Config(
        config=tracer_cfg,
        service_name='GarbageBot',
        validate=True,
    )
    # this call also sets opentracing.tracer
    return config.initialize_tracer()


if __name__ == '__main__':
    # Initializing Jaeger and logging
    log_level = logging.DEBUG
    logging.getLogger('').handlers = []
    logging.basicConfig(format='%(asctime)s %(message)s', level=log_level)
    tracer = init_tracer()
    with tracer.start_span('Garbage Bot Run') as span:
        gls = GitlabServer(tracer=tracer, span=span)
        registry_repos = gls.get_registry_repositories_for_project()
        tag_name_list = []
        branch_slug_list = []
        image_tags_to_delete = []
        tag_list = []
        resp = None

        if gls.settings['dry_run']:
            print('#' * 10 + ' DRY RUN ' + '#' * 10)

        for reg in registry_repos:
            if gls.settings['gl_scope_registry']:
                tag_name_list.append(reg['name'])
            else:
                tag_list += gls.get_image_tags_for_registry_repository(reg['id'])
                for tag in tag_list:
                    tag_name_list.append(tag['name'])
                    tag['reg_id'] = reg['id']

        branches = gls.get_project_branches()
        with tracer.start_span('Normalizing branches', child_of=span) as norm_span:
            for branch in branches:
                branch_slug_list.append(gls.normalize_branch_name(branch.attributes['name'], span=norm_span))

        if gls.settings['gl_add_suffix_to_branch']:
            branch_slug_list_suf = []
            for branch in branch_slug_list:
                branch_slug_list_suf.append(branch + "-" + gls.settings['gl_add_suffix_to_branch'])
            branch_slug_list += branch_slug_list_suf

        if gls.settings['gl_keep_branch_images']:
            image_tags_to_delete = set(tag_name_list) - set(branch_slug_list)
        elif gls.settings['gl_delete_by_regex']:
            image_tags_to_delete = gls.filter_taglist_regex(gls.settings['gl_delete_by_regex'], tag_list)
        elif gls.settings['gl_keep_by_regex']:
            image_tags_to_delete = gls.filter_taglist_regex(gls.settings['gl_keep_by_regex'], tag_list, keep=True)
        elif gls.settings['gl_images_to_delete']:
            image_tags_to_delete = gls.settings['gl_images_to_delete'].split(',')
        elif gls.settings['gl_images_to_keep']:
            image_tags_to_delete = set(tag_name_list) - set(gls.settings['gl_images_to_keep'].split(','))
        elif gls.settings['gl_delete_older_than']:
            for reg in registry_repos:
                with span.tracer.start_span('bulk deleting images', child_of=span) as bulk_span:
                    with bulk_span.tracer.start_span('handling reponse', child_of=bulk_span) as response_span:
                        gls.response_handling_del_request(response=gls.delete_bulk_image_tag(registry_id=reg['id'],
                                                                                             older_than=
                                                                                             gls.settings[
                                                                                                 'gl_delete_older_than'],
                                                                                             span=bulk_span),
                                                          reg_repo_name=reg['location'], scope='bulk', span=response_span)
        elif gls.settings['purge_all']:
            with span.tracer.start_span('purge all active', child_of=span) as pa_span:
                for reg in registry_repos:
                    with pa_span.tracer.start_span('handling reponse', child_of=pa_span) as response_span:
                        gls.response_handling_del_request(response=gls.delete_single_registry_repository(registry_id=reg['id'], span=pa_span),
                                                  reg_repo_name=reg['location'],
                                                  scope='bulk', span=response_span)

        image_tags_to_delete = list(filter(None, image_tags_to_delete))

        with span.tracer.start_span('single deleting images', child_of=span) as del_span:
            for it in image_tags_to_delete:
                with del_span.tracer.start_span('handling reponse for image {0}'.format(it), child_of=del_span) as response_span:
                    print('Deleting image {0}'.format(it))
                    if gls.settings['gl_scope_registry']:
                        for reg in registry_repos:
                            if reg['name'] == it:
                                gls.response_handling_del_request(
                                    response=gls.delete_single_registry_repository(registry_id=reg['id'], span=del_span),
                                    image_tag=it, span=response_span)
                    else:
                        for tag in tag_list:
                            if tag['name'] == it:
                                gls.response_handling_del_request(
                                    response=gls.delete_single_image_tag(registry_id=tag['reg_id'],
                                                                         tag_name=it, span=del_span),
                                                                        image_tag=it, span=response_span)
        print('Done')
    time.sleep(2)
    tracer.close()
